﻿using System;
using System.Threading;
using System.IO.Ports;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/*  Software engineering notes
 *  positions:
 *  
 * 9 Jul 2013 - Next tasks: select a position (1 (blank) 

*/
namespace CSEMFixCom
{
    
    public partial class Form1 : Form
        {
        string version = "0.0.0a 8 Jul 2013";
        string RxString;    // Intended to be the display string during debug
        string TxString;    // for outgoing command
        int RxCtr = 0;      // Counter for characters read from the Serial Port
        string [] RxLines = {"Line 1", "Line 2", "Line 3", "Line 4", "Line 5",
                            "Line 5", "Line 7", "Line 8", "line 9", "line 10"
                            };
        char [] TxChars = new Char[100];
        string NewestLine;
        int RxLindex = 0;

        int [] posn = {1,2,3,4,5,6,7,8};
        string [] ohms = {"10M", "30K", "15K", "10K", "7.5K","5.0K","3.6K","3.0K"};  
        string [] pf = {" 0pf","10pf", "15pf", "22pf", "27pf", "33pf", "47pf"};
        float [] temp = {0, 0, 0, 0, 0, 0, 0, 0};
        float [] Vs = {0, 0, 0, 0, 0, 0, 0, 0};
        float [] Vp = {0, 0, 0, 0, 0, 0, 0, 0};
        float [] F = {0, 0, 0, 0, 0, 0, 0, 0};
        float [] Vm = {0, 0, 0, 0, 0, 0, 0, 0};
        int g_i, current_posn, sensed_posn;
        
        public Form1()
        {
            InitializeComponent();
            this.Text += "     Ver " + version;
            //Serial Port setup . . . 
            ComData = new SerialPort();     // initialize the serial port
            // hook up the Serial Data Received Event Handler . . . .
            ComData.DataReceived += new SerialDataReceivedEventHandler(ComDataHandleIncoming);
            // Set up and open the serial port
            ComData.PortName = "COM1";
            ComData.BaudRate = 9600;
            ComData.DataBits = 8;
            ComData.Parity = Parity.None;
            ComData.StopBits = StopBits.One;
            ComData.Handshake = Handshake.None;
            ComData.RtsEnable = true;       // RTS true enables RS485 Receive
            ComData.DtrEnable = false;      // Initial DTR = false => ~CLK high (in betwn clks)
            // Use DTR sequence of true-false to clock relays
            // for debug
            RxCtr = 0;                      // reset character received counter to zero upon port open
            textBox3.Text = RxCtr.ToString(); // and update the display
            ComData.Open();
            if (ComData.IsOpen)
            {
                textBox2.ReadOnly = false;
                comstatus.Text = "Open";
            }

            show_posn();                        // Display current relay position
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button_send_Click(object sender, EventArgs e) // button_squery (renamed to this)
        {
            //int i;
            ComData.RtsEnable = false;      // enable RS485 transmit
            //TxString = textBox2.Text;
            //TxString += "\r";
            //for (i = 0; i<TxString.Length; i++)
            //    TxChars[i] = TxString[i];
            TxChars[0] = 'R';
            ComData.Write(TxChars, 0, 1);
            Thread.Sleep(4);           // wait x ms to get it out before shutting off RS485
            ComData.RtsEnable = true;       // enable RS485 receive
            
            
            //MessageBox.Show("Send Button Clicked",
            //    "System Debug");
        }

        private void ComDataHandleIncoming(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //MessageBox.Show("Com Data Incomming Triggered",
            //"S Debug n");
            // DEBUG NOTE:  This is what we cannot get to work.
            // RxCtr += ComData.BytesToRead;           // This should count the incoming chars
            RxString = ComData.ReadLine();      // s
            RxCtr += RxString.Length;
            this.Invoke(new EventHandler(Process_EM_Data)); // Cache it away for display & handling.

            //MessageBox.Show("After RxString invoke",
            //    "Sys Debug");
 
        }

        private void Process_EM_Data(object sender, EventArgs e)
        {
            // Show the incoming line of data in textBox3
            int j = RxLindex;
            RxLines[RxLindex++] = RxString;
            NewestLine = RxString;
            RxLindex = RxLindex%5;        // rotate index from 1 to 5

            textBox2.Text = RxLines[(j + 1) %5] + "\r\n" //0ldest line
                + RxLines[(j + 2) %5] + "\r\n"
                + RxLines[(j + 3) %5] + "\r\n"
                + RxLines[(j + 4) %5] + "\r\n"
                + RxLines[(j + 5) %5];        // newest line

            textBox3.Text = RxCtr.ToString();       // and this should update the display count
                
            // parse the Data to it's appropriate location
            // Only process the $D, nVp and nFo readings 
            //RxString.
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ComData.IsOpen) ComData.Close();
            ComData.DataReceived -= ComDataHandleIncoming;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        // Toggle Relay button
        private void button1_Click(object sender, EventArgs e)
        {
            advance_posn();
            show_posn();
            //sensed_posn = emtf_posn();
            //relayPosn_textBox.Text = sensed_posn.ToString();  //
        }

        private void advance_posn()
        {
            // clock the relay counter
            ComData.DtrEnable = true;
            ComData.DtrEnable = false;
        }
        private void show_posn()
        {
            sensed_posn = emtf_posn();
            relayPosn_textBox.Text = sensed_posn.ToString();
            RPosn2.Text = sensed_posn.ToString();
            //relayPosn_textBox.Invoke();
        }

        private int emtf_posn()         // get EM Test Fixture sensed relay active position
        {
            int posn = 0;
            if (!ComData.CtsHolding) posn += 4;     // Note: These lines are inverted by the 
            if (!ComData.DsrHolding) posn += 2;
            if (!ComData.CDHolding) posn += 1;
            return posn;
        }

        private void testcycle()   // to cycle to zero & test                  DEBUG><<<<<<<<<<<<<<<
        {
            // move relay to position 0
            sensed_posn = emtf_posn();
            g_i = 0;
            while ((emtf_posn() != 0) & (g_i++ < 8))
            {
                advance_posn();
                Thread.Sleep(10);
                sensed_posn = emtf_posn();
            }

            if (g_i >= 8)
            {
                MessageBox.Show("Test Fixture NOT Connected!");
                // may want to insert something here to exit application
            }

            Thread.Sleep(1000);
            show_posn();
            advance_posn();

            Thread.Sleep(1000);
            show_posn();
            advance_posn();

            Thread.Sleep(1000);
            show_posn();
            advance_posn();

            Thread.Sleep(1000);
            show_posn();
            advance_posn();

            Thread.Sleep(1000);
            show_posn();
            advance_posn();

            Thread.Sleep(1000);
            show_posn();
            advance_posn();

            Thread.Sleep(1000);
            show_posn();
            advance_posn();

            Thread.Sleep(1000);
            show_posn();
            advance_posn();
        }

        private void Read_EM_Click(object sender, EventArgs e)
        {
            
        }

        // Rotate Relays - test
        private void button1_Click_1(object sender, EventArgs e)
        {
            testcycle();
        }

        private void NRead_Click(object sender, EventArgs e)  // Read the Calibration Values
        {
            int i;
            ComData.RtsEnable = false;      // enable RS485 transmit
            TxString = "$NR\r";
            for (i = 0; i<TxString.Length; i++)
                TxChars[i] = TxString[i];
            ComData.Write(TxChars, 0, TxString.Length);
            //ComData.Write(TxChars, 0, 1);
            Thread.Sleep(5);           // wait 5 ms to get it out before shutting off RS485
            ComData.RtsEnable = true;       // enable RS485 receive
            
        }

        private void NErase_Click(object sender, EventArgs e)   // ERASE Calibration
        {
            int i;
            ComData.RtsEnable = false;      // enable RS485 transmit
            TxString = "$NX\r";
            for (i = 0; i < TxString.Length; i++)
                TxChars[i] = TxString[i];
            ComData.Write(TxChars, 0, TxString.Length);
            //ComData.Write(TxChars, 0, 1);
            Thread.Sleep(5);           // wait 5 ms to get it out before shutting off RS485
            ComData.RtsEnable = true;       // enable RS485 receive
        }

        private void NCalibrate_Click(object sender, EventArgs e)
        {
            int i;
            ComData.RtsEnable = false;      // enable RS485 transmit
            TxString = "$NC\r";
            for (i = 0; i < TxString.Length; i++)
                TxChars[i] = TxString[i];
            ComData.Write(TxChars, 0, TxString.Length);
            //ComData.Write(TxChars, 0, 1);
            Thread.Sleep(5);           // wait 5 ms to get it out before shutting off RS485
            ComData.RtsEnable = true;       // enable RS485 receive
        }

        private void button_pastscan_Click(object sender, EventArgs e) // Kick out pre-adjusted Data
        {
            int i;
            ComData.RtsEnable = false;      // enable RS485 transmit
            TxString = "$P\r";
            for (i = 0; i < TxString.Length; i++)
                TxChars[i] = TxString[i];
            ComData.Write(TxChars, 0, TxString.Length);
            //ComData.Write(TxChars, 0, 1);
            Thread.Sleep(5);           // wait 5 ms to get it out before shutting off RS485
            ComData.RtsEnable = true;       // enable RS485 receive
        }

        private void button_GetVersion_Click(object sender, EventArgs e)
        {
            int i;
            ComData.RtsEnable = false;      // enable RS485 transmit
            TxString = "$V\r";
            for (i = 0; i < TxString.Length; i++)
                TxChars[i] = TxString[i];
            ComData.Write(TxChars, 0, TxString.Length);
            //ComData.Write(TxChars, 0, 1);
            Thread.Sleep(5);           // wait 5 ms to get it out before shutting off RS485
            ComData.RtsEnable = true;       // enable RS485 receive
        }

    

        /* Commands Needed
         * $NC  Normalize Calibrate
         * $NX  Normalize Erase
         * $NR  Normalize Read
         * 
        */ 
    }
}

/*        private void Form1_Load(object sender, EventArgs e)
        {
            TDlistview.Columns.Add("ss");
            TDlistview.View = View.Details;
            ListViewItem item = new ListViewItem("1st Text");
            item.SubItems.Add(new ListViewItem.ListViewSubItem(item, "test"));
            item.SubItems.Add("3");
            item.SubItems.Add("4");
            item.SubItems.Add("5");
            TDlistview.Items.Add(item);
            Thread addThread = new Thread(StartAdding);
            addThread.Start();
        }

       public void StartAdding()
        {
            while (true)
            {
                ListViewItem row = new ListViewItem("Column 1");
                row.SubItems.Add("col-2");
                row.SubItems.Add("col-3");
                row.SubItems.Add("col-4");
                this.Invoke(new Action(() => { TDlistview.Items.Add(row); }));
                Thread.Sleep(1000);
            }
        }
 * 
 * string s = i.ToString();
string s = Convert.ToString(i);
string s = string.Format("{0}", i);
string s = string.Empty + i;
string s = new StringBuilder().Append(i).ToString();

private void ParseEMSData()         // store the incoming data where it goes
        {



        }
 
 */