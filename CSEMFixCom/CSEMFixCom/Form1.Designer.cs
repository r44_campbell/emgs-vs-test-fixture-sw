﻿namespace CSEMFixCom
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_squery = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Toggle_Relay = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comstatus = new System.Windows.Forms.TextBox();
            this.data_textBox = new System.Windows.Forms.TextBox();
            this.relayPosn_textBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.NRead = new System.Windows.Forms.Button();
            this.NErase = new System.Windows.Forms.Button();
            this.NCalibrate = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SerialNumber = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Scan_All_Btn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.RPosn2 = new System.Windows.Forms.Label();
            this.button_pastscan = new System.Windows.Forms.Button();
            this.button_GetVersion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_squery
            // 
            this.button_squery.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_squery.Location = new System.Drawing.Point(16, 270);
            this.button_squery.Name = "button_squery";
            this.button_squery.Size = new System.Drawing.Size(95, 55);
            this.button_squery.TabIndex = 2;
            this.button_squery.Text = "Query for Reading";
            this.button_squery.UseVisualStyleBackColor = true;
            this.button_squery.Click += new System.EventHandler(this.button_send_Click);
            // 
            // textBox2
            // 
            this.textBox2.AcceptsReturn = true;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(11, 106);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox2.Size = new System.Drawing.Size(476, 103);
            this.textBox2.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Incoming Information";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(229, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Com1: 9600, 8, N, 1";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(366, 80);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(88, 22);
            this.textBox3.TabIndex = 8;
            this.textBox3.Text = "Waiting";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(276, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Chars Rc\'d";
            // 
            // Toggle_Relay
            // 
            this.Toggle_Relay.Location = new System.Drawing.Point(565, 51);
            this.Toggle_Relay.Name = "Toggle_Relay";
            this.Toggle_Relay.Size = new System.Drawing.Size(86, 43);
            this.Toggle_Relay.TabIndex = 10;
            this.Toggle_Relay.Text = "Toggle Relay";
            this.Toggle_Relay.UseVisualStyleBackColor = true;
            this.Toggle_Relay.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(229, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Status:";
            // 
            // comstatus
            // 
            this.comstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comstatus.Location = new System.Drawing.Point(270, 25);
            this.comstatus.Name = "comstatus";
            this.comstatus.Size = new System.Drawing.Size(79, 20);
            this.comstatus.TabIndex = 13;
            this.comstatus.Text = "Closed";
            // 
            // data_textBox
            // 
            this.data_textBox.AcceptsReturn = true;
            this.data_textBox.Cursor = System.Windows.Forms.Cursors.UpArrow;
            this.data_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.data_textBox.Location = new System.Drawing.Point(12, 380);
            this.data_textBox.Multiline = true;
            this.data_textBox.Name = "data_textBox";
            this.data_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.data_textBox.Size = new System.Drawing.Size(476, 206);
            this.data_textBox.TabIndex = 14;
            // 
            // relayPosn_textBox
            // 
            this.relayPosn_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.relayPosn_textBox.Location = new System.Drawing.Point(563, 23);
            this.relayPosn_textBox.Name = "relayPosn_textBox";
            this.relayPosn_textBox.Size = new System.Drawing.Size(88, 22);
            this.relayPosn_textBox.TabIndex = 15;
            this.relayPosn_textBox.Text = "Unknown";
            this.relayPosn_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(562, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Relay Position";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(786, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Normalize . . . . . .";
            // 
            // NRead
            // 
            this.NRead.Location = new System.Drawing.Point(683, 51);
            this.NRead.Name = "NRead";
            this.NRead.Size = new System.Drawing.Size(86, 49);
            this.NRead.TabIndex = 18;
            this.NRead.Text = "READ CALIBRATION";
            this.NRead.UseVisualStyleBackColor = true;
            this.NRead.Click += new System.EventHandler(this.NRead_Click);
            // 
            // NErase
            // 
            this.NErase.Location = new System.Drawing.Point(789, 51);
            this.NErase.Name = "NErase";
            this.NErase.Size = new System.Drawing.Size(86, 51);
            this.NErase.TabIndex = 19;
            this.NErase.Text = "ERASE CALIBRATION";
            this.NErase.UseVisualStyleBackColor = true;
            this.NErase.Click += new System.EventHandler(this.NErase_Click);
            // 
            // NCalibrate
            // 
            this.NCalibrate.Location = new System.Drawing.Point(891, 51);
            this.NCalibrate.Name = "NCalibrate";
            this.NCalibrate.Size = new System.Drawing.Size(86, 49);
            this.NCalibrate.TabIndex = 20;
            this.NCalibrate.Text = "DO CALIBRATION";
            this.NCalibrate.UseVisualStyleBackColor = true;
            this.NCalibrate.Click += new System.EventHandler(this.NCalibrate_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(877, 154);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(886, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Calibration Values";
            // 
            // SerialNumber
            // 
            this.SerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SerialNumber.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.SerialNumber.Location = new System.Drawing.Point(213, 241);
            this.SerialNumber.MaxLength = 6;
            this.SerialNumber.Name = "SerialNumber";
            this.SerialNumber.Size = new System.Drawing.Size(147, 26);
            this.SerialNumber.TabIndex = 23;
            this.SerialNumber.WordWrap = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 247);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(196, 20);
            this.label9.TabIndex = 24;
            this.label9.Text = "EM Sensor Serial Number:";
            // 
            // Scan_All_Btn
            // 
            this.Scan_All_Btn.Location = new System.Drawing.Point(565, 123);
            this.Scan_All_Btn.Name = "Scan_All_Btn";
            this.Scan_All_Btn.Size = new System.Drawing.Size(86, 43);
            this.Scan_All_Btn.TabIndex = 25;
            this.Scan_All_Btn.Text = "SCAN ALL";
            this.Scan_All_Btn.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(563, 214);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 43);
            this.button1.TabIndex = 26;
            this.button1.Text = "Rotate Relays";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // RPosn2
            // 
            this.RPosn2.AutoSize = true;
            this.RPosn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RPosn2.Location = new System.Drawing.Point(666, 1);
            this.RPosn2.Name = "RPosn2";
            this.RPosn2.Size = new System.Drawing.Size(115, 24);
            this.RPosn2.TabIndex = 27;
            this.RPosn2.Text = "Relay Posn";
            // 
            // button_pastscan
            // 
            this.button_pastscan.Location = new System.Drawing.Point(683, 123);
            this.button_pastscan.Name = "button_pastscan";
            this.button_pastscan.Size = new System.Drawing.Size(93, 43);
            this.button_pastscan.TabIndex = 28;
            this.button_pastscan.Text = "RE-READ LAST SCAN";
            this.button_pastscan.UseVisualStyleBackColor = true;
            this.button_pastscan.Click += new System.EventHandler(this.button_pastscan_Click);
            // 
            // button_GetVersion
            // 
            this.button_GetVersion.Location = new System.Drawing.Point(789, 123);
            this.button_GetVersion.Name = "button_GetVersion";
            this.button_GetVersion.Size = new System.Drawing.Size(86, 43);
            this.button_GetVersion.TabIndex = 29;
            this.button_GetVersion.Text = "GET SW VERSION";
            this.button_GetVersion.UseVisualStyleBackColor = true;
            this.button_GetVersion.Click += new System.EventHandler(this.button_GetVersion_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 621);
            this.Controls.Add(this.button_GetVersion);
            this.Controls.Add(this.button_pastscan);
            this.Controls.Add(this.RPosn2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Scan_All_Btn);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.SerialNumber);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.NCalibrate);
            this.Controls.Add(this.NErase);
            this.Controls.Add(this.NRead);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.relayPosn_textBox);
            this.Controls.Add(this.data_textBox);
            this.Controls.Add(this.comstatus);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Toggle_Relay);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button_squery);
            this.Name = "Form1";
            this.Text = "EM Grain Sensor R6-1 Test";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.IO.Ports.SerialPort ComData;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Toggle_Relay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox comstatus;
        private System.Windows.Forms.TextBox data_textBox;
        private System.Windows.Forms.TextBox relayPosn_textBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button NRead;
        private System.Windows.Forms.Button NErase;
        private System.Windows.Forms.Button NCalibrate;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox SerialNumber;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button Scan_All_Btn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label RPosn2;
        private System.Windows.Forms.Button button_squery;
        private System.Windows.Forms.Button button_pastscan;
        private System.Windows.Forms.Button button_GetVersion;
    }
}

